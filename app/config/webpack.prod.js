const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const autoprefixer = require('autoprefixer');
const globImporter = require('node-sass-glob-importer');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// App configuration.
const config = require('./config');
// Common configuration.
const common = require('./webpack.common');

module.exports = merge(common, {
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                  fallback: 'style-loader',
                  use: [
                    {
                      loader: 'css-loader',
                      options: {
                        sourceMap: true
                      }
                    },
                    {
                      loader: 'postcss-loader',
                      options: {
                        plugins: () => [autoprefixer()]
                      }
                    },
                    {
                      loader: 'sass-loader',
                      options: {
                        importer: globImporter(),
                        includePaths: ['./node_modules'],
                        sourceMap: true
                      },
                    }
                  ]
                })
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css'),
        new UglifyJSPlugin()
    ]
});

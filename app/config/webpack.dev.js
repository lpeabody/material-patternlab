const path = require('path');
const globImporter = require('node-sass-glob-importer');
const autoprefixer = require('autoprefixer');
const common = require('./webpack.common');
const merge = require('webpack-merge');
const webpack = require('webpack');
const { VIRTUAL_HOST } = process.env;

let local_config = {
  virtual_host_port: 32780
};
try {
  local_config = require('./local-config');
  local_config.virtual_host = VIRTUAL_HOST;
} catch (ex) {
  console.log('No local config is defined.');
}

module.exports = merge(common, {
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {loader: 'style-loader'},
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [autoprefixer()]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              importer: globImporter(),
              includePaths: ['./node_modules'],
              sourceMap: true
            },
          }
        ]
      },
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    allowedHosts: ['.docksal'],
    contentBase: path.resolve(__dirname, '../../public'),
    host: '0.0.0.0',
    open: false,
    public: `http://${local_config.virtual_host}:${local_config.virtual_host_port}`,
    watchContentBase: true,
    hot: true
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  }
});

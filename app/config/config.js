const src = './pl/source';
const dest = './public/dist';

module.exports = {
    app: src,
    build: dest,
    scripts: {
        src: `${src}/js/app.js`,
        dest: `${dest}/js`,
    }
};

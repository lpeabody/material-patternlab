const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const config = require('./config');
const path = require('path');

module.exports = {
  context: path.resolve(config.app),
  entry: {
    app: './js/app'
  },
  output: {
    chunkFilename: '[name].bundle.js',
    filename: '[name].built.js',
    path: path.resolve(config.build),
    publicPath: '/dist/'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
        },
      }
    ]
  },
  plugins: [
      new CleanWebpackPlugin()
  ]
};

# Material Pattern Lab

Pattern Lab project dedicated to building basic web components using the Material Design Components NPM library. Development is conslidated between Gulp and Webpack. It is recommended that you run the project through Docksal.

- [Material Design Guidelines](https://material.io/design/)
- [Material Design Web Components and Documentation](https://material.io/develop/web/)
- [Getting started with Material Components](https://material.io/develop/web/docs/getting-started/)
- [Material Components Web Github](https://github.com/material-components/material-components-web)

## Setup

1. Install Docksal.
2. Clone project.
3. Run `fin init`.
4. Navigate to the URL indicated at the end of the init script.

## Development

Development is run through [webpack-dev-server](https://webpack.js.org/configuration/dev-server/).

### Watched Files

Currently, the only watched files are:

1. pl/source/css/style.scss
    - Any SCSS file imported by style.scss. Recursive.
2. pl/source/js/app.js

### Starting the Server

Assuming you have setup the project:

1. Run `fin exec npm start`.
2. Navigate to the URL/port indicated in the webpack output.
3. Edit files listed in the [Watched Files](#markdown-header-watched-files) section.

*Note: When the project is first initializaed, it creates local-config.js in app/config/local-config.js. This currently only stores the port used to connect to the dev server. Every time the Docksal cli service is restarted, a new port will be mapped to port 8080 (what webpack-dev-server runs on internally). If you restart the cli service, you can run `fin port`, and copy/paste the port in local-config.js. You can then start the dev server to continue your work.* 

### Compiling Pattern Lab

Pattern Lab compiles the source directory into the public directory. Compiling Pattern Lab can be done via:

```
fin plab
```
or
```
fin exec /var/www/pl/core/console --generate
```

Webpack Dev Server is configured to watch the content base (public). This means if you recompile Pattern Lab, public will be replaced with the latest generated version, and Webpack Dev Server will reload the page for you automatically.

## Docksal Commands

### fin gen

This will re-compile Pattern Lab and run the production webpack build. You will be able to visit http://material-patternlab.docksal/ to view the Pattern Lab without having to run the dev server.

### fin plab

Just recompiles Pattern Lab.

### fin port

Outputs the externally mapped port to port 8080. Useful if you've restarted the project services and need to update the port assignment in local-config.js.

### fin init

Re-initializes the project. 

1. Re-creates all containers.
2. Installs Composer dependencies.
3. Installs NPM packages.
4. Hard Git reset (make sure to commit your changes if you need to, they will be lost after running `init`).
5. Compiles Pattern Lab.
6. Runs a production webpack build.

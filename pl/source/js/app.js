import autoInit from '@material/auto-init';
import {MDCTopAppBar} from '@material/top-app-bar';
import {MDCList} from '@material/list';
import {MDCRipple} from '@material/ripple';
import {MDCDrawer} from "@material/drawer";

autoInit.register('MDCTopAppBar', MDCTopAppBar);
autoInit.register('MDCRipple', MDCRipple);
autoInit.register('MDCList', MDCList);
autoInit.register('MDCDrawer', MDCDrawer);

autoInit();

const drawer = MDCDrawer.attachTo(document.querySelector('.sidebar-one-col'));
const topAppBar = MDCTopAppBar.attachTo(document.getElementById('app-bar'));
topAppBar.setScrollTarget(document.getElementById('main-content'));
topAppBar.listen('MDCTopAppBar:nav', () => {
  drawer.open = !drawer.open;
  console.log('toggle hit');
});

console.log('hey');

import style from '../css/style.scss';

if (module.hot) {
  module.hot.accept();
}
